/*
	Created: MEF - January 2017 for first release
	Purpose: Receive trigger events and pass the handling of those events
			 to the AwardTriggerHandler class
*/
trigger Award on Award__c (after insert, after update, before delete, 
before insert) {

AwardTriggerHandler handler = new AwardTriggerHandler();
	
	if(Trigger.isBefore && Trigger.isInsert) {
		handler.onBeforeInsert(Trigger.new, Trigger.newMap);
	}
	
	if(Trigger.isBefore && Trigger.isUpdate) {
		handler.onBeforeUpdate(Trigger.new, Trigger.newMap, Trigger.oldMap);
	}
	
	if(Trigger.isAfter && Trigger.isInsert) {
		handler.onAfterInsert(Trigger.new, Trigger.newMap);
	}
	
	if(Trigger.isAfter && Trigger.isUpdate) {
		handler.onAfterUpdate(Trigger.new, Trigger.old, Trigger.newMap, Trigger.oldMap);
	}
}