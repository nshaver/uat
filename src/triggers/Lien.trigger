/*
	Created: RAP - December 2016 for first release
	Purpose: PD-319 - Create Trigger for Lien Insert

	Updated: RAP - January 2017 for first release
	Purpose: Add setting of recordtype, move processing to utility class 
*/
trigger Lien on Lien__c (before insert, before update) {
    
    if (trigger.isBefore) 
   		LienUtility.ProcessLiens(trigger.new, trigger.oldMap);
}