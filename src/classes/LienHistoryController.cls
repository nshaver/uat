/*
    Created: RAP - January 2017 for March Release
    Purpose: PD-82 - MVP - History Tracking
*/

public with sharing class LienHistoryController { 
    
    public string lienId{get;set;}
    
    public LienHistoryController(ApexPages.StandardController stdCtrl) {
		lienId = stdCtrl.getId();
    }
    
    public list<Lien__History> lienH {
    	get {
   			return [SELECT OldValue, NewValue, Field, CreatedById, CreatedDate, Id, ParentId, CreatedBy.Name 
   					FROM Lien__History
   					WHERE ParentId = :lienId
   					order by CreatedDate desc];
    	}
    }
}