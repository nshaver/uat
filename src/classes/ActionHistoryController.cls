/*
    Created: RAP - February 2017 for March Release
    Purpose: PD-435 - Action - Provide a detail view of all field level changes
*/

public with sharing class ActionHistoryController { 
    
    public string actionId{get;set;}
    
    public ActionHistoryController(ApexPages.StandardController stdCtrl) {
		actionId = stdCtrl.getId();
    }
    
    public list<Action__History> actionH {
    	get {
   			return [SELECT OldValue, NewValue, Field, CreatedById, CreatedDate, Id, ParentId, CreatedBy.Name 
   					FROM Action__History
   					WHERE ParentId = :actionId
   					order by CreatedDate desc];
    	}
    }
}