/*
    Created: RAP - March 2017 for March Release
    Purpose: test functions of History Controllers
    		 Coverage as of 3/6 - ActionHistoryController: 100%
    		 					  ClaimantHistoryController: 100%
    		 					  LienHistoryController: 100%
*/
@isTest

private class HistoryController_Test {

@testSetup
	static void CreateData() {
    	Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('LawFirm').getRecordTypeId();
        Account acc = new Account(Name = 'Law Firm',
        						  RecordTypeId = rtId,
        						  Priority_Firm__c = true, 
                                  BillingStreet = '123 Main St',
                                  BillingCity = 'Denver',
                                  BillingState = 'CO',
                                  BillingPostalCode = '80202');
        insert acc;
        Action__c action = new Action__c(Name = 'Test Action',
                                         Active__c = false);
        insert action;
        action.Law_Firm__c = acc.Id;
        update action;
        action.Active__c = true;
        update action;
        Claimant__c claim = new Claimant__c(Address__c = '123 Main St',
                                            City__c = 'Denver',
                                            Email__c = 'rap@rap.com',
                                            Law_Firm__c = acc.Id,
                                            First_Name__c = 'RAP',
                                            Last_Name__c = 'RAPPER',
                                            Phone__c = '3035551212',
                                            SSN__c = '135461448',
                                            State__c = 'CO',
                                            Status__c = 'Cleared',
                                            Zip__c = '80138');
        insert claim;
        claim.First_Name__c = 'RAPPER';
        update claim;
        claim.Last_Name__c = 'RAP';
        update claim;
        Lien__c lien = new Lien__c(Account__c = acc.Id, 
                                   Action__c = action.Id, 
                                   Claimant__c = claim.Id, 
                                   Cleared__c = false,
                                   Date_Submitted__c = system.today(), 
                                   Lien_Type__c = 'Private', 
                                   Notes__c = 'This is a note', 
                                   Stages__c = 'Submitted', 
                                   State__c = 'CO', 
                                   Submitted__c = true);
		insert lien;
		lien.State__c = 'AZ';
		update lien;
		lien.Cleared__c = true;
		update lien;
	}
	
	static testMethod void TestHistories() {
		Lien__c l = [SELECT Id, Claimant__r.Id, Action__r.Id FROM Lien__c WHERE Lien_Type__c = 'Private' limit 1];
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(l.Action__r);
		ActionHistoryController ctrl = new ActionHistoryController(stdCtrl);
		list<Action__History> aH = ctrl.actionH;
		
		stdCtrl = new ApexPages.StandardController(l.Claimant__r);
		ClaimantHistoryController ctrl2 = new ClaimantHistoryController(stdCtrl);
		list<Claimant__History> cH = ctrl2.claimantH;

		stdCtrl = new ApexPages.StandardController(l);
		LienHistoryController ctrl3 = new LienHistoryController(stdCtrl);
		list<Lien__History> lH = ctrl3.lienH;
	}
}