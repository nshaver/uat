/*
    Created - RAP February 2017 for March Release
    Purpose - PD-431 - Release Dates Auto-Move (Batch processor grabs all Awards belonging to an Action with a Distribution 
    		  Date of the day it runs, collects amounts available for disbursement, and moves those funds out of availability
*/
global class DistributionBatch implements Database.Batchable<Award__c>, Database.Stateful {

    public Date queryDate{get;set;}
	public string action{get;set;}
	public integer numReleases{get;set;}
    public DistributionBatch(string act) {
    	queryDate = system.today();
		numReleases = 0;
    	action = act;
    }
    
	global Iterable<Award__c> start(Database.BatchableContext BC) {
		DisbursementDates__c ddc = DisbursementDates__c.getInstance(action);
		if (ddc == null || ddc.Date__c != queryDate)
			return null;
		return [SELECT Amount__c, Assoc_Costs_Award_Deductions__c, Id, Amount_Applied__c, Amount_Disbursed__c,
					   (SELECT Lien__c, Lien__r.HBAmount__c, Lien__r.Total_Fees_Applied__c, Lien__r.Total_Fees_Paid__c
					    FROM Liens__r 
					    WHERE Lien__r.Stages__c = 'Final')
				FROM Award__c
				WHERE Action__c = :action
				AND (Distribution_Status__c = 'Cleared'
				OR Distribution_Status__c = 'Released_to_a_Holdback')];
	}

	public void execute(Database.BatchableContext BC, list<Award__c> awards) {
		list<Release__c> releases = new list<Release__c>();
		for (Award__c a : awards) {
			decimal hold = a.Amount_Applied__c + a.Assoc_Costs_Award_Deductions__c + a.Amount_Disbursed__c;
			for (AwardLien__c al : a.Liens__r) {
				decimal unpaid = al.Lien__r.HBAmount__c + al.Lien__r.Total_Fees_Applied__c - al.Lien__r.Total_Fees_Paid__c;
				hold += unpaid;
			}
			if (a.Amount__c > hold) {
				decimal sendAmt = a.Amount__c - hold;
				Release__c send = new Release__c(Award__c = a.Id,
												 Amount__c = sendAmt,
												 Date__c = queryDate);
				releases.add(send);
			}
		}
		if (!releases.isEmpty()) {
			insert releases;
			numReleases += releases.size();
		}
	}
	
	public void finish(Database.BatchableContext BC) {
		system.debug(loggingLevel.INFO, 'RAP --->> Number of Releases created: ' + numReleases);
	}
}