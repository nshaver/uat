<!--
    Created: RAP - February 2017 for March Release
    Purpose: PD-173 - Claimant - Award Tab
-->
<apex:page standardController="Claimant__c" extensions="AwardListController" standardStylesheets="false" docType="HTML">
	<apex:stylesheet value="{!$Resource.SLDS221}/assets/styles/salesforce-lightning-design-system-vf.css" />
	<apex:outputPanel styleclass="IFS slds-scrollable--x">
		<apex:dataTable value="{!awList}" var="a" width="100%" columns="8" footerclass="IFS slds-truncate" rules="rows"
			styleclass="IFS slds-table slds-table--bordered" headerClass="IFS slds-col--padded slds-text-title--caps" >
			<apex:column headerValue="Description" >
				<apex:outputLink value="/{!a.idVal}" styleclass="IFS slds-truncate">
					{!a.description}
				</apex:outputLink>
	            <apex:facet name="footer" >
					<apex:outputText value="Totals:" styleclass="IFS slds-truncate" />
	            </apex:facet>
			</apex:column>		
			<apex:column headerValue="Award Value" >
				<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!a.amount < 1000}">
					<apex:param value="{!a.amount}" />
				</apex:outputText>
				<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!a.amount >= 1000}">
					<apex:param value="{!a.amount}" />
				</apex:outputText>
	            <apex:facet name="footer" >
					<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!totalAwards < 1000}">
						<apex:param value="{!totalAwards}" />
					</apex:outputText>
					<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!totalAwards >= 1000}">
						<apex:param value="{!totalAwards}" />
					</apex:outputText>
	            </apex:facet>
			</apex:column>		
			<apex:column headerValue="Assoc Costs" >
				<apex:outputPanel rendered="{!a.totalCosts != null && a.totalCosts > 0}" >
					<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!a.totalCosts < 1000}">
						<apex:param value="{!a.totalCosts}" />
					</apex:outputText>
					<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!a.totalCosts >= 1000}">
						<apex:param value="{!a.totalCosts}" />
					</apex:outputText>
				</apex:outputPanel>
	            <apex:facet name="footer" >
					<apex:outputPanel rendered="{!totalCosts != null && totalCosts > 0}" >
						<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!totalCosts < 1000}">
							<apex:param value="{!totalCosts}" />
						</apex:outputText>
						<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!totalCosts >= 1000}">
							<apex:param value="{!totalCosts}" />
						</apex:outputText>
					</apex:outputPanel>
	            </apex:facet>
			</apex:column>		
			<apex:column headerValue="Rel Injuries" >
				<apex:repeat value="{!a.injuries}" var="i" rendered="{!a.showInjuries}">
					<apex:outputLink value="/{!i.Injury__c}" target="_blank" styleclass="IFS slds-truncate">
						{!i.Injury__r.Name}
					</apex:outputLink>
					<br/>
				</apex:repeat>
			</apex:column>		
			<apex:column headerValue="Related Liens" >
				<apex:repeat value="{!a.liens}" var="l" rendered="{!a.showLiens}">
					<apex:outputLink value="/{!l.Lien__c}" target="_blank" styleclass="IFS slds-truncate">
						{!l.Lien__r.Name}
					</apex:outputLink>
					<br/>
				 </apex:repeat>
			</apex:column>		
			<apex:column headerValue="Lien Amts" >
				<apex:repeat value="{!a.liens}" var="g" rendered="{!a.showLiens}">
					<apex:outputPanel rendered="{!g.LienAmountCovered__c == null || g.LienAmountCovered__c = 0}" >
						<center><apex:outputText value="-" styleclass="IFS slds-truncate" /></center>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!g.LienAmountCovered__c != null && g.LienAmountCovered__c > 0}">
						<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!g.LienAmountCovered__c < 1000}">
							<apex:param value="{!g.LienAmountCovered__c}" />
						</apex:outputText>
						<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!g.LienAmountCovered__c >= 1000}">
							<apex:param value="{!g.LienAmountCovered__c}" />
						</apex:outputText>
						<br/>
					</apex:outputPanel>
				</apex:repeat>
	            <apex:facet name="footer" >
					<apex:outputPanel rendered="{!totalLiens != null && totalLiens > 0}">
						<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!totalLiens < 1000}">
							<apex:param value="{!totalLiens}" />
						</apex:outputText>
						<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!totalLiens >= 1000}">
							<apex:param value="{!totalLiens}" />
						</apex:outputText>
					</apex:outputPanel>
	            </apex:facet>
			</apex:column>		
			<apex:column headerValue="Holdbacks">
				<apex:repeat value="{!a.liens}" var="h" rendered="{!a.showLiens}">
					<apex:outputPanel rendered="{!h.Lien__r.HBAmount__c == null || h.Lien__r.HBAmount__c == 0}" >
						<center><apex:outputText value="-" styleclass="IFS slds-truncate" /></center>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!h.Lien__r.HBAmount__c != null && h.Lien__r.HBAmount__c > 0}">
						<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!h.Lien__r.HBAmount__c < 1000}">
							<apex:param value="{!h.Lien__r.HBAmount__c}" />
						</apex:outputText>
						<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!h.Lien__r.HBAmount__c >= 1000}">
							<apex:param value="{!h.Lien__r.HBAmount__c}" />
						</apex:outputText>
						<br/>
					</apex:outputPanel>
				</apex:repeat>
	            <apex:facet name="footer" >
					<apex:outputPanel rendered="{!totalHoldbacks != null && totalHoldbacks > 0}" >
						<apex:outputText value="${0, number, 00.00}" styleclass="IFS slds-truncate" rendered="{!totalHoldbacks < 1000}">
							<apex:param value="{!totalHoldbacks}" />
						</apex:outputText>
						<apex:outputText value="${0, number, 0,000.00}" styleclass="IFS slds-truncate" rendered="{!totalHoldbacks >= 1000}">
							<apex:param value="{!totalHoldbacks}" />
						</apex:outputText>
					</apex:outputPanel>
	            </apex:facet>
			</apex:column>		
			<apex:column headerValue="Status">
				<apex:outputText value="{!a.status}" styleclass="IFS slds-truncate" />
			</apex:column>		
		</apex:dataTable>
	</apex:outputPanel>
</apex:page>