/*  
    Created: RAP - January 2017 for March Release
    Purpose: PD-348 - Display data for just one Action

History:

    Updated: lmsw - February 2017 for March Release
    Purpose: PD-436 -Modify as Parent component
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getClaimantStatusCount");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var arrayOfMapKeys = [];
                var storeResponse = response.getReturnValue();
                component.set("v.statusCountMap", storeResponse);
                component.set("v.actionID", component.get("v.recordId"))
                for(var singleKey in storeResponse) {
                    arrayOfMapKeys.push(singleKey);
                }
                component.set('v.lstKey', arrayOfMapKeys);
            } else {
                console.log('Problem getting Claimant Status Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
                
        var action2 = component.get("c.getDistributionStatusCount");
        action2.setParams({"aId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                var arrayOfMapKeys = [];
                var storeResponse = response.getReturnValue();
                component.set("v.distributionCountMap", storeResponse);
                for(var singleKey in storeResponse) {
                    arrayOfMapKeys.push(singleKey);
                }
                component.set('v.lstKeyD', arrayOfMapKeys);
            } else {
                console.log('Problem getting Distribution Status Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);
    }
    
})