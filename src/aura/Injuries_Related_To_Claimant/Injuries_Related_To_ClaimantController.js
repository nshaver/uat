({
    doInit : function(component, event, helper) {
        var action = component.get("c.getInjuries2");
        action.setParams({"cId": component.get("v.recordId")});
        // action.setParams({"sId": component.get("v.claimant.action__c")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.injuries", response.getReturnValue());
            } else {
                console.log('Problem getting injuries, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

        var action2 = component.get("c.getAwardsByInjuryMap");
        action2.setParams({"cId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.awardsperinjury", response.getReturnValue());
            } else {
                console.log('Problem getting injuries, response state: ' + state);
            }
        });
        $A.enqueueAction(action2);
    }
, 
    editInjury : function(component, event, helper) {
        var injuryrowID = event.target.id;
        var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
          "recordId": injuryrowID
       	});
        editRecordEvent.fire();
    } 
,
  	createInjury : function (component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Injury__c"
        });
        createRecordEvent.fire();
        }

})