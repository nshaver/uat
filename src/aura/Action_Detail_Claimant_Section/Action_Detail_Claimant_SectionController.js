({
    doInit : function(component, event, helper) {
        var action = component.get("c.getClaimants");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.claimants", response.getReturnValue());
            } else {
                console.log('Problem getting claimants, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }
})