/*
    Created: lmsw - February 2017 for March Release
    Purpose: PD-348 - Display Claimant status for an action
*/
({
	statusLabel : function(component) {
		// Get Label of Status picklist
		var selected = component.get("v.key");
		selected = selected.replace(/_/g," ");
		component.set("v.keyLabel", selected);
	},
	getCPercent : function(component) {
		var aID = component.get("v.actionID");
		var value = component.get("v.value");
		var percent = 0;
		var action = component.get("c.getClaimantsCountForAction");
		action.setParams({"aId": aID});
		action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
            	var total = response.getReturnValue();
            	if(total>0){ percent = Math.round((value/total)*100); } 
				component.set("v.percentC", percent);                       
            } else {
                console.log('Problem getting Total Claimants Count, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	}
})