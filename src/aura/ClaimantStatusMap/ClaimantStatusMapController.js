/* 
    Created: lmsw - February 2017 for March Release
    Purpose: PD-348 - Display Claimant status for an action
*/
({
	doInit : function(component, event, helper) {
        var key = component.get("v.key");
        var map = component.get("v.map");
        
        component.set("v.value" , map[key]);       
        helper.statusLabel(component); 
        helper.getCPercent(component);
	}       
})