/*  
	Created: lmsw - February 2017 for March Release
    Purpose: PD-436 -Modify as Parent component

*/
({
    getActionRecord : function(component,event,helper){
        var action = component.get("c.getAction");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.currentAction", response.getReturnValue());
            } else {
                console.log('Problem getting Action, response state: ' + state);
            }
        });     
        $A.enqueueAction(action);
    }
})