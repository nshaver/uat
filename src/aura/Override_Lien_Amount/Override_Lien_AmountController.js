({

    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienamts", response.getReturnValue());
            } else {
                console.log('Problem getting lien amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);

        var action2 = component.get("c.getLien");
        action2.setParams({"lId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lien", response.getReturnValue());
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
        });
        
        $A.enqueueAction(action2);
    }

, 
	editLienAmount : function(component, event, helper) {
	    var lienamountrowID = event.target.id;
	    var editRecordEvent = $A.get("e.force:editRecord");
	        editRecordEvent.setParams({
	      		"recordId": lienamountrowID
	   		});
	    editRecordEvent.fire();
    
    }
,
    showOverride : function(component, event, helper) {
        var cmpMsg = component.find("override-section");
        $A.util.removeClass(cmpMsg, 'hide');

        var overridebtn= component.find("override-btn-id");
        $A.util.addClass(overridebtn, 'hide');

    }
,
    hideOverride : function(component, event, helper) {
        var cmpMsg = component.find("override-section");
        $A.util.addClass(cmpMsg, 'hide');

        var overridebtn= component.find("override-btn-id");
        $A.util.removeClass(overridebtn, 'hide');
    }
,
    saveOverride : function(component, event, helper) {
        var lienId = component.get("v.recordId");  
       //var today = Now();
        var reasons = component.find("lienOverrideReason").get("v.value");
        var amount = component.find("lienOverrideAmount").get("v.value");

        var action4 = component.get("c.UpdateLien");
        action4.setParams({
            "lId"  : lienId,
            "reason"  : reasons,
            "amount"  : amount
        });
        action4.setCallback(component, function(response) {
            var state = response.getState();
            if (component.isValid() && state ==="SUCCESS") {
                component.set("v.lien", response.getReturnValue());
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
            $A.get("e.force:refreshView").fire();
            var cmpMsg = component.find("override-section");
            $A.util.addClass(cmpMsg, 'hide');

            var overridebtn= component.find("override-btn-id");
            $A.util.removeClass(overridebtn, 'hide');
        });
        $A.enqueueAction(action4);

    }

})