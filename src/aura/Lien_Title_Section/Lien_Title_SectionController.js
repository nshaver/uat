/* 
    Created: Marc Dysart - January 2017 for first release
    Purpose: PD-304 - Lien Title Section Component for Lien 

History:

    Updated: lmsw - January 2017 for March Release
    Purpose: PD-399 - Add Conga button to Lien page 

    Update: lmsw - February 2017 for March Release
    Purpose: PD-399 - Modify Conga Composer Window to recommended size
             PRODSUPT-8 - Change name in title
    
*/
({

    doInit : function(component, event, helper) {
        helper.getLienRecord(component);
// PRODSUPT-8
        helper.getLienTypes(component);
    },
    editLien : function(component, event, helper) { 
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
          "recordId": component.get("v.lien.Id")
        });
        editRecordEvent.fire();
    },
    generateConga : function(component,event, helper) {
        // Get lien information for template
        var lienId = component.get("v.lien.Id");
        var acctId = component.get("v.lien.Account__c");
        var claimantId = component.get("v.lien.Claimant__c");
        // Get Conga Custom Labels to build link
        var congaLink = $A.get("$Label.c.Conga_baseUrl")+'--apxtconga4.'+$A.get("$Label.c.Conga_sfInstance")+'.visual.force.com/apex/Conga_Composer';
        var recordLink = '?id='+lienId;
        var lienField = $A.get("$Label.c.Conga_lienField_LD")+lienId;
        var acctField = $A.get("$Label.c.Conga_acctField_LD")+acctId;
        var claimantField = $A.get("$Label.c.Conga_claimantField_LD")+claimantId;
        var congaUrl = congaLink+recordLink+'&QueryId='+acctField+','+claimantField+','+lienField;
        console.log("congaUrl = "+ congaUrl);
// Modify conga composer window
        window.open(congaUrl, '_blank',"height=660px width=900px");
        /*Conga button link ex:
            https://providio--lisa--apxtconga4.cs11.visual.force.com/apex/Conga_Composer
            ?id=a08Z0000006PNbo
            &QueryId=[ALI]a0IZ0000003xjw1?pv0=001Z000001Ds4kj,
            [II]a0IZ0000003xjwV?pv0=a0BZ000000BwWNc,
            [LI]a0IZ0000003xjx0?pv0=a08Z0000006PNbo
        */
   }
})