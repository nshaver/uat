({
    doInit : function(component, event, helper) {
        var action = component.get("c.getAction");
        action.setParams({"aId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.action", response.getReturnValue());
            } else {
                console.log('Problem getting action, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        console.log('RAP--->> response = ' + response);
    },

    editAction : function(component, event, helper) { 
    	var editRecordEvent = $A.get("e.force:editRecord");
    	editRecordEvent.setParams({
    		"recordId": component.get("v.recordId")
    	});
    editRecordEvent.fire();
}
    
})