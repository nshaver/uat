/*  Created: lmsw - January 2017 first draft
    Purpose: PD-168 - Title Section on the Claimant Detail Page

History:

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-1 - Modified for change of claimant status picklist

	Updated: lmsw - February 2017 for March Release
	Purpose: PD-168 - Modified for changes in .css, removed console.logs

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-1 Change Claimant Status: 'Partially Cleared'=> 'Released to a Holdback'

	Updated: lmsw - February 2017 for March Release
	Purpose: PRODSUPT-14 - remove "Send Email" button

 */
({
	getClaimantRecord : function(component) {
		var action = component.get("c.getClaimant");
		action.setParams({"cId": component.get("v.recordId")});
		action.setCallback(this, function(response) {
			var state = response.getState();
			if(component.isValid() && state ==="SUCCESS") {
				component.set("v.claimant", response.getReturnValue());
//				this.enableEmailButton(component);
				this.statusCircle(component);
			} else {
				console.log('Problem getting claimant, response state: ' + state);
			}
		});
		$A.enqueueAction(action);
	},
// Peer code review change - Modified logic for color of circle	
	statusCircle : function(component) {
		// Determine color of circle
		var selected = component.find("ClaimantStatusSelect").get("v.value");
// start PRODSUPT-1 PD-168
		selected = selected.replace(/ /g,"_");
		if(selected == "Cleared") {
			component.set("v.circleStyle", "defaultCircle greenCircle");
		} else if(selected == "Released_to_a_Holdback") {
			component.set("v.circleStyle", "defaultCircle yellowCircle");
		} else {
			component.set("v.circleStyle", "defaultCircle redCircle");
		} 
	},
// end PRODSUPT-1 PD-168
// start PRODSUPT-14
/*	enableEmailButton : function(component) {
		var lawFirmEmail = component.get("v.claimant.Law_Firm__r.Email__c");
		if(lawFirmEmail) { component.set("v.hasEmail","true"); }
			else{ component.set("v.hasEmail", "false");	}
	} */
// end PRODSUPT-14	
})