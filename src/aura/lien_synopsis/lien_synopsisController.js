/* 
History:

    Updated: lmsw - February 2017 for March Release
    Purpose: PD-393 Add synopsis to title. 
             PRODSUPT-9
*/
({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLienAmtList");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienAmts", response.getReturnValue());
            } else {
                console.log('Problem getting lien Amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        helper.getLienStageLabels(component);
        helper.getLienSubstageLabels(component);
    },
    editLien : function(component, event, helper) { 
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
          "recordId": component.get("v.lien.Id")
        });
        editRecordEvent.fire();
    }
})