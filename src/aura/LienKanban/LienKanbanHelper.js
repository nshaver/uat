({
	hideCards : function(component) {
		 // This shows all the cards first if they were hidden
        var cards = document.getElementsByClassName("hideCard");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.remove("hideCard")
        }

        // Then hides all the cards

        var cards = document.getElementsByClassName("lien-card");
        for(var i = 0; i < cards.length; ++i){
            cards.item(i).classList.add("hideCard")
        }
	},

	saveNewStage : function(component, newstage, lienId) {
        var action5 = component.get("c.updateLienCard");
        action5.setParams({
            "lId" : lienId,
            "stage"  : newstage
        });   
        $A.enqueueAction(action5);
    },

     // This shows only the appropriate cards
    showCorrectCards : function(component, cardName) {
    	var cards = document.getElementsByClassName(cardName);
	        for(var i = 0; i < cards.length; ++i){
	            cards.item(i).classList.remove("hideCard")
	        }
    },

    // This changes the button to look selected
    // This can be deleted if it is confirmed that no Filter buttons are needed
    showButtonSelected: function(component, event) {
        var cards = document.getElementsByClassName("slds-button--brand");
            for(var i = 0; i < cards.length; ++i){
                cards.item(i).classList.remove("slds-button--brand")
                cards.item(i).classList.add("slds-not-selected")
            }
        var thisbutton= event.target;
        $A.util.removeClass(thisbutton, 'slds-not-selected');
        $A.util.addClass(thisbutton, 'slds-button--brand'); 
    },

    updateTriggerLabel: function(cmp, event) {
        var triggerCmp = cmp.find("trigger");
        if (triggerCmp) {
            var source = event.getSource();
            var label = source.get("v.label");
            triggerCmp.set("v.label", label);
        }
    }

})